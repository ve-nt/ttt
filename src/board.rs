use std::error::Error;
use std::fmt::{self, Display};
use std::iter::{self, Iterator};
use std::ops::Deref;

const TERM_CLEAR: &str = "\x1B[2J";
const TERM_CURSOR_RESET: &str = "\x1B[1;1H";

pub struct Board {
    width: usize,
    height: usize,
    in_a_row: usize,
    board: Vec<Vec<BoardSquare>>,
}

impl Board {
    pub fn new(width: usize, height: usize, in_a_row: usize) -> Result<Self, BoardError> {
        if width < 3 || height < 3 {
            return Err(BoardError::BoardTooSmall(width, height));
        }

        if in_a_row < 3 {
            return Err(BoardError::InARowTooSmall(in_a_row));
        }

        if in_a_row > width && in_a_row > height {
            return Err(BoardError::InARowTooBigForBoard(in_a_row, (width, height)));
        }

        let board = vec![vec![BoardSquare::empty(); width]; height];

        Ok(Self {
            width,
            height,
            in_a_row,
            board,
        })
    }

    pub fn place(&mut self, piece: &Piece, x: usize, y: usize) -> Result<(), BoardError> {
        if x == 0 || y == 0 || x > self.width || y > self.height {
            Err(BoardError::OutOfBounds((x, y)))
        } else if let Some(p) = *self.board[y - 1][x - 1] {
            Err(BoardError::SquareAlreadyTaken(p, (x, y)))
        } else {
            self.board[y - 1][x - 1] = BoardSquare::from(*piece);
            Ok(())
        }
    }

    pub fn is_full(&self) -> bool {
        self.board
            .iter()
            .flatten()
            .find(|square| square.is_vacant())
            .is_none()
    }

    pub fn winner(&self) -> Option<Piece> {
        [self.rows(), self.cols(), self.diags()]
            .iter()
            .flatten()
            .map(|line| self.check_winner(&line))
            .find_map(|w| w)
    }

    fn rows(&self) -> Vec<Vec<BoardSquare>> {
        self.board.clone()
    }

    fn cols(&self) -> Vec<Vec<BoardSquare>> {
        (0..self.width)
            .map(|i| {
                self.board
                    .iter()
                    .map(|d| d[i])
                    .collect::<Vec<BoardSquare>>()
            })
            .collect()
    }

    fn diags(&self) -> Vec<Vec<BoardSquare>> {
        [self.diags_nw_se(), self.diags_sw_ne()].concat()
    }

    fn diags_nw_se(&self) -> Vec<Vec<BoardSquare>> {
        Self::zig_zag(&self.board)
    }

    fn diags_sw_ne(&self) -> Vec<Vec<BoardSquare>> {
        Self::zig_zag(
            self.board
                .iter()
                .rev()
                .cloned()
                .collect::<Vec<Vec<BoardSquare>>>()
                .as_ref(),
        )
    }

    fn zig_zag(board: &[Vec<BoardSquare>]) -> Vec<Vec<BoardSquare>> {
        let mut zig_zag: Vec<Vec<BoardSquare>> = Vec::new();
        let mut coords: Vec<(usize, usize)> = Vec::new();

        coords.push((0, 0));
        while !coords.is_empty() {
            let mut zig: Vec<BoardSquare> = Vec::new();
            let mut next_coords: Vec<(usize, usize)> = Vec::new();

            for &(row, col) in &coords {
                zig.push(board[row][col]);

                if row + 1 < board.len() && !next_coords.contains(&(row + 1, col)) {
                    next_coords.push((row + 1, col));
                }

                if col + 1 < board[0].len() && !next_coords.contains(&(row, col + 1)) {
                    next_coords.push((row, col + 1));
                }
            }

            coords = next_coords;
            zig_zag.push(zig);
        }

        zig_zag
    }

    fn check_winner(&self, pieces: &[BoardSquare]) -> Option<Piece> {
        let mut count = 1;

        let mut iter = pieces.iter().peekable();
        loop {
            let &curr = iter.next()?;
            let &next = iter.peek()?;

            count = match (*curr, **next) {
                (Some(c), Some(n)) => {
                    if c == n {
                        count + 1
                    } else {
                        1
                    }
                }
                (None, Some(_)) => 1,
                _ => 0,
            };

            if count >= self.in_a_row {
                return *curr;
            }
        }
    }

    fn header(&self) -> String {
        format!("┌{}┐\n", self.frame_horz())
    }

    fn body(&self) -> String {
        self.board
            .iter()
            .rev()
            .map(|row| {
                format!(
                    "│ {}│\n",
                    row.iter()
                        .map(|square| format!("{} ", square))
                        .collect::<String>()
                )
            })
            .collect()
    }

    fn footer(&self) -> String {
        format!("└{}┘", self.frame_horz())
    }

    fn frame_horz(&self) -> String {
        iter::repeat('─').take(self.width * 2 + 1).collect()
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "{}{}{}{}{}",
            TERM_CLEAR,
            TERM_CURSOR_RESET,
            self.header(),
            self.body(),
            self.footer()
        )
    }
}

#[derive(Clone, Copy, Debug)]
struct BoardSquare(Option<Piece>);

impl BoardSquare {
    pub fn empty() -> Self {
        BoardSquare(None)
    }

    pub fn from(piece: Piece) -> Self {
        BoardSquare(Some(piece))
    }

    pub fn is_vacant(&self) -> bool {
        self.0.is_none()
    }
}

impl Deref for BoardSquare {
    type Target = Option<Piece>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl Display for BoardSquare {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.0 {
            Some(p) => write!(f, "{}", p),
            None => write!(f, "·"),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Piece {
    Nought,
    Cross,
}

impl Display for Piece {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Self::Nought => "O",
            Self::Cross => "X",
        };

        write!(f, "{}", s)
    }
}

#[derive(Debug)]
pub enum BoardError {
    SquareAlreadyTaken(Piece, (usize, usize)),
    OutOfBounds((usize, usize)),
    BoardTooSmall(usize, usize),
    InARowTooSmall(usize),
    InARowTooBigForBoard(usize, (usize, usize)),
}

impl Display for BoardError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Self::OutOfBounds((x, y)) => format!("Coordinate '({},{})' is out of bounds", x, y),
            Self::BoardTooSmall(w, h) => format!("Board size {{{},{}}} is too small", w, h),
            Self::InARowTooSmall(i) => format!("In-a-row value '{}' too small (minimum 3)", i),
            Self::SquareAlreadyTaken(piece, (x, y)) => {
                format!("({},{}) is already taken by {}", x, y, piece)
            }
            Self::InARowTooBigForBoard(i, (w, h)) => {
                format!(
                    "In-a-row value '{}' too big for board size {{{},{}}}",
                    i, w, h
                )
            }
        };

        write!(f, "{}", s)
    }
}

impl Error for BoardError {}
