use structopt::StructOpt;

#[derive(StructOpt)]
#[structopt(about = "A Tic-Tac-Toe Implementation")]
pub struct Opt {
    /// Width of the game board.
    #[structopt(short, default_value = "3")]
    pub width: usize,

    /// Height of the game board.
    #[structopt(short, default_value = "3")]
    pub height: usize,

    /// How many in-a-rows a winner needs.
    #[structopt(short = "n", default_value = "3")]
    pub in_a_row: usize,
}

impl Opt {
    pub fn get() -> Self {
        Opt::from_args()
    }
}
