mod board;
mod opt;

use board::{Board, Piece};
use scan_fmt::{scan_fmt, scanln_fmt};
use std::io::Write;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Gathe CLI args from the user.
    let opt = opt::Opt::get();

    // Initialize our board, players and our turn counter.
    let mut board = Board::new(opt.width, opt.height, opt.in_a_row).unwrap_or_else(|e| {
        eprintln!("{}", e);
        std::process::exit(1);
    });

    let mut player = [Piece::Nought, Piece::Cross].iter().cycle();
    let mut turn = player.next().unwrap();

    println!("{}", board);
    loop {
        // Print the prompt for the user.
        print!("{}> ", turn);
        std::io::stdout().lock().flush()?;

        // Read in the coordinates from the user.
        let (x, y) = match scanln_fmt!("{} {}", usize, usize) {
            Ok((x, y)) => (x, y),
            Err(_) => {
                eprintln!("Invalid format: Expected format '<x> <y>' ");
                continue;
            }
        };

        // Handle if we need to move to the next player or not.
        match board.place(turn, x, y) {
            Ok(_) => {
                turn = player.next().unwrap();
                println!("{}", board);
            }
            Err(e) => {
                eprintln!("{}", e);
                continue;
            }
        }

        // If we have a winner, congratulate them and exit.
        if let Some(winner) = board.winner() {
            println!("{} wins!", winner);
            std::process::exit(0);
        }

        // Check for a draw scenario.
        if board.is_full() {
            println!("draw!");
            std::process::exit(0);
        }
    }
}
